cat > main.py <<EOF
#!/usr/bin/python
############################################################
#
# Copyright $(date +%Y)
# TODO:Add License Here if needed, or a License.txt to
#      project
#
############################################################
if __name__ != '__main__':
  raise Exception("Not meant to be included as a module")

import argparse
import logging
import os

parser = argparse.ArgumentParser()
parser.add_argument('--example', '-e')
parser.add_argument('--verbose', '-v')
parser.add_argument('--debug', '-d')

args = parser.parse_args()

default_loglevel = logging.ERROR
if args.verbose:
  default_loglevel = logging.INFO
if args.debug:
  default_loglevel = logging.DEBUG

logging.baseConfig(level=default_loglevel)
