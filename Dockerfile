ARG PYTHON_VERSION=3
FROM python:${PYTHON_VERSION}-alpine

RUN pip install --upgrade pipenv\
 && addgroup user\
 && adduser -h /home -s /bin/bash -D -G user user\
 && apk add --update\
            alpine-sdk\
            bash\
            docker-bash-completion\
            docker-vim\
            docker\
            mariadb-dev\
            nodejs\
            postgresql-dev\
            sqlite\
            openssh\
            vim\
 && mkdir -p /project\
 && chown -R user:user /project

ADD root_dir /
USER user
WORKDIR /project
ENV SHELL=/bin/bash
VOLUME ["/project"]
ENTRYPOINT ["/docker-entrypoint.sh"]
